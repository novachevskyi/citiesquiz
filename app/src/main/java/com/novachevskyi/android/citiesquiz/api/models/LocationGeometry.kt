package com.novachevskyi.android.citiesquiz.api.models

data class LocationGeometry(
        val location: LocationModel
)
