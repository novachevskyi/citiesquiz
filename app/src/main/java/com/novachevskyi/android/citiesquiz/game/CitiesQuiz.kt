package com.novachevskyi.android.citiesquiz.game

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class CitiesQuiz
