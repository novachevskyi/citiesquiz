package com.novachevskyi.android.citiesquiz.game

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.novachevskyi.android.citiesquiz.R
import com.novachevskyi.android.citiesquiz.api.LocationsService
import com.novachevskyi.android.citiesquiz.api.models.LocationModel
import com.novachevskyi.android.citiesquiz.models.CitiesQuizGameStateResults
import javax.inject.Inject

private val EUROPE_BOUNDS = LatLngBounds.Builder()
        .include(LatLng(66.322568, -28.447196))
        .include(LatLng(35.800471, 51.512382))
        .build()

@CitiesQuiz
class CitiesQuizPresenter
@Inject internal constructor(
        val viewModel: CitiesQuizViewModel,
        private val gameStateController: CitiesQuizGameStateController,
        private val locationsService: LocationsService
) {

    private lateinit var context: Context
    private var map: MapboxMap? = null

    private var progressDialog: ProgressDialog? = null

    fun present(context: Context) {
        this.context = context

        resetGame()

        viewModel.onPlaceClick
                .doOnNext { loadCityLocation() }
                .subscribe()
    }

    fun configureMap(map: MapboxMap) {
        this.map = map
        map.setLatLngBoundsForCameraTarget(EUROPE_BOUNDS)
        map.setMaxZoomPreference(4.0)
        map.setMinZoomPreference(3.0)

        map.addOnMapClickListener {
            clearAllMarkers()

            gameStateController.selectedLat = it.latitude
            gameStateController.selectedLon = it.longitude

            map.addMarker(
                    MarkerOptions().position(it)
            )
        }
    }

    private fun clearAllMarkers() {
        map?.markers?.forEach {
            map?.removeMarker(it)
        }
    }

    private fun loadCityLocation() {
        if (gameStateController.selectedLat == 0.0 && gameStateController.selectedLon == 0.0) {
            AlertDialog.Builder(context)
                    .setTitle(context.resources.getString(R.string.select_title))
                    .setMessage(context.resources.getString(R.string.select_message))
                    .setPositiveButton(context.resources.getString(android.R.string.ok), { _, _ -> })
                    .show()
            return
        }

        locationsService.getCityCoordinates(gameStateController.currentCity)
                .doOnSubscribe { showProgress() }
                .doOnError { showError() }
                .doOnSuccess {
                    if (it == null) {
                        showError()
                    } else {
                        placePoint(it)
                    }
                }
                .subscribe()
    }

    private fun placePoint(point: LocationModel) {
        val result = gameStateController.placePoint(point)
        showRoundResult(result)
        displayGameState()

        map?.addMarker(
                MarkerOptions().position(
                        LatLng(point.lat, point.lng)
                )
        )
    }

    private fun showRoundResult(result: CitiesQuizGameStateResults) {
        progressDialog?.hide()

        val title = when (result.gameState) {
            CitiesQuizGameStateEnum.ROUND_SUCCESS -> context.resources.getString(R.string.success_title)
            CitiesQuizGameStateEnum.ROUND_FAIL -> context.resources.getString(R.string.fail_title)
            CitiesQuizGameStateEnum.GAME_SUCCESS -> context.resources.getString(R.string.game_success_title)
            CitiesQuizGameStateEnum.GAME_FAIL -> context.resources.getString(R.string.game_fail_title)
        }

        val message = when (result.gameState) {
            CitiesQuizGameStateEnum.ROUND_SUCCESS -> context.resources.getString(R.string.success_message)
            CitiesQuizGameStateEnum.ROUND_FAIL -> context.resources.getQuantityString(R.plurals.fail_message, result.distance, result.distance)
            CitiesQuizGameStateEnum.GAME_SUCCESS -> context.resources.getQuantityString(R.plurals.game_success_message, gameStateController.count, gameStateController.count)
            CitiesQuizGameStateEnum.GAME_FAIL -> context.resources.getQuantityString(R.plurals.game_fail_message, result.distance, result.distance)
        }

        AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.resources.getString(android.R.string.ok), { _, _ ->
                    if (result.gameState == CitiesQuizGameStateEnum.GAME_SUCCESS || result.gameState == CitiesQuizGameStateEnum.GAME_FAIL) {
                        resetGame()
                    }
                })
                .show()
    }

    private fun showError() {
        progressDialog?.hide()

        AlertDialog.Builder(context)
                .setTitle(context.resources.getString(R.string.error_title))
                .setMessage(context.resources.getString(R.string.error_message))
                .setPositiveButton(context.resources.getString(android.R.string.ok), { _, _ -> })
                .show()
    }

    private fun showProgress() {
        progressDialog = ProgressDialog.show(context, context.resources.getString(R.string.progress_title),
                context.resources.getString(R.string.progress_message), true)
    }

    private fun resetGame() {
        gameStateController.reset()
        displayGameState()
        clearAllMarkers()
    }

    private fun displayGameState() {
        viewModel.counter.set(
                context.resources.getQuantityString(
                        R.plurals.cities_placed_title, gameStateController.count, gameStateController.count
                )
        )

        viewModel.distance.set(
                context.resources.getQuantityString(
                        R.plurals.distance_title, gameStateController.distance, gameStateController.distance
                )
        )

        viewModel.location.set(
                context.resources.getString(
                        R.string.location_title, gameStateController.currentCity
                )
        )
    }
}
