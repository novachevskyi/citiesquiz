package com.novachevskyi.android.citiesquiz

import com.novachevskyi.android.citiesquiz.api.ApiModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MainModule::class, ApiModule::class))
interface MainComponent {

    fun inject(activity: MainApplication)

    fun inject(activity: MainActivity)
}
