package com.novachevskyi.android.citiesquiz.api

import com.novachevskyi.android.citiesquiz.api.models.LocationsResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Single

interface LocationsApi {

    @GET("maps/api/geocode/json?sensor=false&key=AIzaSyBRH_rxW88p6PsnHlRZiKIUQINPWVQszUA")
    fun getLocationInfo(@Query("address") city: String): Single<LocationsResponse>
}
