package com.novachevskyi.android.citiesquiz.models

data class CityModel(
    val capitalCity: String
)
