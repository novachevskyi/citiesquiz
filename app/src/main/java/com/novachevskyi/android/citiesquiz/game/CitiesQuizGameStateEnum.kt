package com.novachevskyi.android.citiesquiz.game

enum class CitiesQuizGameStateEnum {
    ROUND_SUCCESS,
    ROUND_FAIL,
    GAME_SUCCESS,
    GAME_FAIL
}
