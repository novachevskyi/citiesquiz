package com.novachevskyi.android.citiesquiz.models

data class CitiesQuizModel(
    val capitalCities: List<CityModel>
)
