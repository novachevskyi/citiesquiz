package com.novachevskyi.android.citiesquiz.api.models

data class LocationModel(
        val lat: Double,
        val lng: Double
)
