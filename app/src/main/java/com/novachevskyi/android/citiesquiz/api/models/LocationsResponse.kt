package com.novachevskyi.android.citiesquiz.api.models

data class LocationsResponse(
        val results: List<LocationResponse>
)
