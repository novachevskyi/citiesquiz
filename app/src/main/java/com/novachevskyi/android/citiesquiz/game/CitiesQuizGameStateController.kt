package com.novachevskyi.android.citiesquiz.game

import android.location.Location
import com.novachevskyi.android.citiesquiz.models.CitiesQuizGameStateResults
import com.novachevskyi.android.citiesquiz.models.CityModel
import com.novachevskyi.android.citiesquiz.api.models.LocationModel
import javax.inject.Inject

@CitiesQuiz
class CitiesQuizGameStateController
@Inject internal constructor(
        private val service: CitiesQuizTaskStorage
) {

    internal var selectedLat: Double = 0.0
    internal var selectedLon: Double = 0.0

    private lateinit var cities: MutableList<CityModel>

    internal var distance: Int = 0
    internal var count: Int = 0
    internal lateinit var currentCity: String

    fun reset() {
        selectedLat = 0.0
        selectedLon = 0.0

        cities = service.loadCities().capitalCities.toMutableList()

        distance = 1500
        count = 0
        currentCity = getNextCity()
    }

    fun placePoint(point: LocationModel): CitiesQuizGameStateResults {
        val distanceInKilometers = getDistanceInKilometersFromSelectedPoint(point)
        val result = distanceInKilometers <= 50

        if (!result) {
            if (distanceInKilometers > distance) {
                return CitiesQuizGameStateResults(CitiesQuizGameStateEnum.GAME_FAIL, distanceInKilometers, point.lat, point.lng)
            }

            distance -= distanceInKilometers
        } else {
            count += 1
        }

        if (cities.size == 0) {
            return CitiesQuizGameStateResults(CitiesQuizGameStateEnum.GAME_SUCCESS, distanceInKilometers, point.lat, point.lng)
        }

        currentCity = getNextCity()

        return if (result) CitiesQuizGameStateResults(CitiesQuizGameStateEnum.ROUND_SUCCESS, distanceInKilometers, point.lat, point.lng)
        else CitiesQuizGameStateResults(CitiesQuizGameStateEnum.ROUND_FAIL, distanceInKilometers, point.lat, point.lng)
    }

    private fun getDistanceInKilometersFromSelectedPoint(point: LocationModel): Int {
        val selectedLocation = Location("").apply {
            latitude = selectedLat
            longitude = selectedLon
        }

        val realLocation = Location("").apply {
            latitude = point.lat
            longitude = point.lng
        }

        return (selectedLocation.distanceTo(realLocation) / 1000).toInt()
    }

    private fun getNextCity(): String {
        val city = cities.firstOrNull()
        cities.remove(city)
        return city?.capitalCity ?: "Not Defined"
    }
}
