package com.novachevskyi.android.citiesquiz.game

import android.content.Context
import com.google.gson.Gson
import com.novachevskyi.android.citiesquiz.R
import com.novachevskyi.android.citiesquiz.models.CitiesQuizModel
import javax.inject.Inject

@CitiesQuiz
class CitiesQuizTaskStorage
@Inject internal constructor(
        private val context: Context,
        private val gson: Gson
) {

    fun loadCities(): CitiesQuizModel =
            gson.fromJson(loadCitiesQuizJsonFromAsset(), CitiesQuizModel::class.java)

    private fun loadCitiesQuizJsonFromAsset(): String =
            context.resources.openRawResource(R.raw.capital_cities)
                    .bufferedReader().use { it.readText() }
}
