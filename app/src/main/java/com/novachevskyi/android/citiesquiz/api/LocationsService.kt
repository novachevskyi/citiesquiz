package com.novachevskyi.android.citiesquiz.api

import com.novachevskyi.android.citiesquiz.game.CitiesQuiz
import com.novachevskyi.android.citiesquiz.api.models.LocationModel
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@CitiesQuiz
class LocationsService
@Inject internal constructor(
        private val api: LocationsApi
) {

    fun getCityCoordinates(city: String): Single<LocationModel?> =
            api.getLocationInfo(city)
                    .observeOn(AndroidSchedulers.mainThread())
                    .map { it.results.firstOrNull()?.geometry?.location }
}
