package com.novachevskyi.android.citiesquiz

import android.app.Application

class MainApplication : Application() {

    companion object {
        @JvmStatic lateinit var graph: MainComponent
    }

    override fun onCreate() {
        super.onCreate()

        graph = DaggerMainComponent.builder()
                .mainModule(MainModule(this))
                .build()
        graph.inject(this)
    }
}
