package com.novachevskyi.android.citiesquiz

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView
import com.novachevskyi.android.citiesquiz.databinding.ActivityMainBinding
import com.novachevskyi.android.citiesquiz.game.CitiesQuizPresenter
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var presenter: CitiesQuizPresenter

    private lateinit var mapView: MapView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainApplication.graph.inject(this)

        Mapbox.getInstance(this, "pk.eyJ1Ijoibm92YWNoZXZza3lpIiwiYSI6ImNqZW82OW85bTAyNDIyeW5xNjlid3JxMDUifQ.zuCgsL0cb0dawxhGdx7Kbw")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = presenter.viewModel
        binding.executePendingBindings()

        mapView = binding.mapView
        mapView.getMapAsync { presenter.configureMap(it) }
        mapView.onCreate(savedInstanceState)

        presenter.present(this)
    }

    public override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    public override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    public override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    public override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }
}
