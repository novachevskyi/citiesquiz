package com.novachevskyi.android.citiesquiz

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application
}
