package com.novachevskyi.android.citiesquiz.models

import com.novachevskyi.android.citiesquiz.game.CitiesQuizGameStateEnum

data class CitiesQuizGameStateResults(
        val gameState: CitiesQuizGameStateEnum,
        val distance: Int,
        val cityLat: Double,
        val cityLon: Double
)
