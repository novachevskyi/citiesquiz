package com.novachevskyi.android.citiesquiz.api.models

data class LocationResponse(
        val geometry: LocationGeometry
)
