package com.novachevskyi.android.citiesquiz.game

import android.databinding.ObservableField
import rx.subjects.PublishSubject
import javax.inject.Inject

@CitiesQuiz
class CitiesQuizViewModel
@Inject internal constructor() {

    val counter = ObservableField<String>()
    val distance = ObservableField<String>()
    val location = ObservableField<String>()

    val onPlaceClick: PublishSubject<Unit> = PublishSubject.create()

    fun placePoint() {
        onPlaceClick.onNext(Unit)
    }
}
